rm-network:
	docker network rm conference-go

create-network:
	docker network create --driver bridge conference-go

start-rabbit-mq:
	docker run -d --rm --hostname rabbitmq --network conference-go --name rabbitmq rabbitmq:3

build:
	docker build -f monolith/Dockerfile.dev ./monolith -t conference-go-dev
	docker build -f attendees_microservice/Dockerfile.dev ./attendees_microservice -t attendees-microservice-dev
	docker build -f presentation_workflow/Dockerfile.dev ./presentation_workflow -t presentation-workflow-dev 
	docker build -f attendees_microservice/Dockerfile.account_info.dev ./attendees_microservice -t attendees-account-info-dev 

run:
	docker run -d --rm -v "$(PWD)/monolith:/app" -p 8000:8000 --network conference-go --name monolith conference-go-dev
	docker run -d --rm -v "$(PWD)/attendees_microservice:/app" -p 8001:8001 --network conference-go --name attendees-microservice attendees-microservice-dev
	docker run -d --rm -v "$(PWD)/presentation_workflow:/app" --network conference-go --name presentation-workflow presentation-workflow-dev
	docker run -d --rm -v "$(PWD)/attendees_microservice:/app" --network conference-go --name attendees-account-info attendees-account-info-dev bash

start-mailer:
	docker run -d --rm -p "3000:8025" -e "MH_SMTP_BIND_ADDR=0.0.0.0:25" --network conference-go --name mail mailhog/mailhog