import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_rejection(ch, method, properties, body):
    #  "presenter_name": presentation.presenter_name,
    #           "presenter_email": presentation.presenter_email,
    #           "title": presentation.title,
    clue = json.loads(body)
    name = clue["presenter_name"]
    title = clue["title"]
    email = clue["presenter_email"]

    send_mail(
        "Your presentation has been rejected",
        f"{name}, we're sorry to tell you that your presentation {title} has been rejected",
        "admin@conference.go",
        [email],
        fail_silently=False,
    )
    print("Rejection Email Sent")
    print("END CONSUMING")


def process_approval(ch, method, properties, body):
    #  "presenter_name": presentation.presenter_name,
    #           "presenter_email": presentation.presenter_email,
    #           "title": presentation.title,
    clue = json.loads(body)
    name = clue["presenter_name"]
    title = clue["title"]
    email = clue["presenter_email"]

    send_mail(
        "Your presentation has been accepted",
        f"{name}, we're happy to tell you that your presentation {title} has been accepted",
        "admin@conference.go",
        [email],
        fail_silently=False,
    )
    print("Approval Email Sent")
    print("END CONSUMING")


def processing_queue():
    print("START CONSUMING")
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="approval_tasks")
    channel.queue_declare(queue="rejection_tasks")
    channel.basic_consume(
        queue="approval_tasks",
        on_message_callback=process_approval,
        auto_ack=True,
    )
    channel.basic_consume(
        queue="rejection_tasks",
        on_message_callback=process_rejection,
        auto_ack=True,
    )
    channel.start_consuming()


if __name__ == "__main__":
    try:
        while True:
            try:
                processing_queue()
            except AMQPConnectionError:
                print("Could not connect to RabbitMQ")
                time.sleep(2.0)
    except KeyboardInterrupt:
        print("Interrupted")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
